import React, { useState } from 'react';
import { Button, FlatList, ScrollView, StyleSheet, Text, ToastAndroid, View, Image, TextInput } from 'react-native';

// como hacer un request
const requestHTTP = async () => {

  try {

    var response = await fetch('https://bitbucket.org/itesmguillermorivas/partial2/raw/45f22905941b70964102fce8caf882b51e988d23/carros.json');

    var json = await response.json();

    alert(json);
    console.log(json);
    console.log(json[0]);
    console.log(json[0].marca);
  }catch(error) {

    console.error(error);
  }
}

const alertita = (mensaje:string) => {

  alert(mensaje);
  console.log(mensaje);
  requestHTTP();

  // codigo específico por plataforma
  // ToastAndroid.show(mensaje, ToastAndroid.SHORT);
}

const Perrito = (props:any) => {

  // props - argumentos que recibimos del exterior y personalizan el componente
  // estados - variables internas de un componente que pueden cambiar
  
  // const [nombre_de_variable, funcion_modificadora] = useState(valor_default)
  const [estaFeliz, setEstaFeliz] = useState(false);
  const [textito, setTextito] = useState("");

  return(
    <View>
      <Text>woof! {props.nombre} y estoy {estaFeliz? "MUY FELIZ! :)" : "MUY TRISTE :("} </Text>
      <Button  
        title="cambiar de estado de animo"
        onPress={() => {
          setEstaFeliz(!estaFeliz);
        }}
      />
      <TextInput 
        placeholder="introduce texto aqui"
        onChangeText={text => {setTextito(text)}}
        defaultValue={textito}
      />
      <Text>woof! {textito} </Text>
      <Button  
        title="mostrar estado de animo"
        onPress={() => {
          alertita(estaFeliz + "");
        }}
      />
    </View>

  );  
}

const App = () => {

  // JSX
  // javascript xml
  return(
    <View style={estilo.contenedor}>
      <FlatList 
        data={[
          {key: 'Fifi L'},
          {key: 'Fido L'},
          {key: 'Firulais L'},
          {key: 'Killer L'},
          {key: 'Kaiser L'}
        ]}
        renderItem={({item}) => <Perrito nombre={item.key} />}
      />
      <ScrollView>
        <Image
          source={{uri: "http://cdn.edgecast.steamstatic.com/steamcommunity/public/images/avatars/b3/b3a58499b3d0046dddb08f089f7639111e75216e_full.jpg"}}
          style={{width: 200, height: 200}}
        />
        <Image
          source={{uri: "http://cdn.edgecast.steamstatic.com/steamcommunity/public/images/avatars/b3/b3a58499b3d0046dddb08f089f7639111e75216e_full.jpg"}}
          style={{width: 200, height: 200}}
        />
        <Image
          source={{uri: "http://cdn.edgecast.steamstatic.com/steamcommunity/public/images/avatars/b3/b3a58499b3d0046dddb08f089f7639111e75216e_full.jpg"}}
          style={{width: 200, height: 200}}
        />
        <Image
          source={{uri: "http://cdn.edgecast.steamstatic.com/steamcommunity/public/images/avatars/b3/b3a58499b3d0046dddb08f089f7639111e75216e_full.jpg"}}
          style={{width: 200, height: 200}}
        />
        <Image
          source={{uri: "http://cdn.edgecast.steamstatic.com/steamcommunity/public/images/avatars/b3/b3a58499b3d0046dddb08f089f7639111e75216e_full.jpg"}}
          style={{width: 200, height: 200}}
        />
      </ScrollView>
    </View>
  );

}

// definición de hoja de estilo
const estilo = StyleSheet.create({
  contenedor: {

    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },  
});

export default App;